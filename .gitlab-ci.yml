stages:
  - build
  - qemu
  - package

.build_vars:
  variables:
    TEST_REPO: 'https://github.com/elisa-tech/meta-elisa.git'
    TEST_BRANCH: 'master'

.build_setup:
  before_script:
    - cp -r /repo/.repo* .
    - export PATH=$PATH:$(pwd)/.repo/repo

meta-elisa-build:
  stage: build
  image: registry.gitlab.com/elisa-tech/docker-image/elisa
  tags:
    - elisa
  extends:
    - .build_vars
    - .build_setup
  script:
    - repo init -b needlefish -u https://gerrit.automotivelinux.org/gerrit/AGL/AGL-repo
    - repo sync
    - git clone -b ${TEST_BRANCH} ${TEST_REPO}
    - source meta-agl/scripts/aglsetup.sh -f elisa-cluster-demo
    - echo "DL_DIR = \"/download\"" >> conf/local.conf
    - echo "SSTATE_MIRRORS += \"file://.* https://elisa-builder-00.iol.unh.edu/sstate/needlefish/PATH\"" >> conf/local.conf
    - echo "INHERIT += \"create-spdx\"" >> conf/local.conf
    - echo "SPDX_PRETTY = \"1\"" >> conf/local.conf
    - bitbake elisa-cluster-demo-platform
    - mkdir ../image
    - mkdir ../sbom
    - cp -L tmp/deploy/images/qemux86-64/elisa-cluster-demo-platform-qemux86-64.ext4 ../image/.
    - cp -L tmp/deploy/images/qemux86-64/bzImage ../image/.
    - cp -L tmp/deploy/images/qemux86-64/elisa-cluster-demo-platform-qemux86-64.spdx* ../sbom/.
    - gzip -r ../image
  artifacts:
    expire_in: 1 day
    paths:
      - image
      - sbom

qemu-boot:
  stage: qemu
  image: elisa:qemu-ci
  variables:
    GIT_STRATEGY: none
  tags:
    - elisa
  script:
    - |
      gunzip -r image
      mkdir screenshot
      cd image
      mkfifo /tmp/guest.in /tmp/guest.out
      qemu-system-x86_64 -snapshot -device virtio-net-pci,netdev=net0,mac=52:54:00:12:35:02 \
        -watchdog i6300esb  -netdev user,id=net0,hostfwd=tcp::2222-:22,hostfwd=tcp::2323-:23 \
        -drive file=elisa-cluster-demo-platform-qemux86-64.ext4,if=virtio,format=raw  -usb \
        -device usb-tablet -device virtio-rng-pci -vga virtio  -machine q35 -cpu kvm64 \
        -cpu qemu64,+ssse3,+sse4.1,+sse4.2,+popcnt -enable-kvm -m 4096  -smp 4 -m 2048 \
        -serial pipe:/tmp/guest -kernel bzImage \
        -append 'root=/dev/vda rw  console=ttyS0 mem=4096M ip=dhcp oprofile.timer=1 console=ttyS0,115200n8 loglevel=7' &
      while read line; do
        echo "${line}"
        if [[ ${line} == *"Automotive Grade Linux"* ]]; then
                echo "" > /tmp/guest.in
                sleep 5
                continue
        fi
        if [[ ${line} == *"qemux86-64 login:"* ]]; then
                sleep 5
                echo "root" > /tmp/guest.in
                sleep 10
                echo "" > /tmp/guest.in
                sleep 5
                echo "" > /tmp/guest.in
                vncsnapshot :0 ../screenshot/screenshot_$(date +"%Y%m%d_%H%M%S").jpeg
                echo "Signalsource-control-panel" > /tmp/guest.in
                sleep 5
                echo "" > /tmp/guest.in
                sleep 5
                vncsnapshot :0 ../screenshot/screenshot_$(date +"%Y%m%d_%H%M%S").jpeg
                exit 0
        fi
      done < /tmp/guest.out
  artifacts:
    expire_in: 1 day
    paths:
      - screenshot/*

push_package:
  stage: package
  image: debian:bullseye-slim
  needs: ["meta-elisa-build"]
  script:
    - apt-get update
    - apt-get install -y curl xz-utils tar
    - tar -cJvf image.tar.xz image
    - |
      curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "image.tar.xz" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/image/1/image.tar.xz"
  only:
    - main
