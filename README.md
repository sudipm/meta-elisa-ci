# meta-elisa-ci

This builds meta-elisa from https://github.com/elisa-tech/meta-elisa/

To test your changes, edit the line `git clone` in .gitlab-ci.yml file to clone
your repo with the changes to meta-elisa. The ci will build and give the image.
